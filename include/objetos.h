//En este archivo ud podra definir los objetos que podrán ser usados 
//con el slab allocator. Declare tambien el constructor y destructor de 
//cada objeto declarado. Abajo se provee un objeto de ejemplo.
//Declare al menos tres objetos distintos

//El constructor siempre recibe un puntero tipo void al tipo de objeto,
//y su tamaño (tal como se declaro el puntero a funcion).
//No olvide implementar las funciones de crear y destruir de cada objeto.

typedef struct ejemplo{
    int a;
    float b[100];
    char *msg;
    unsigned int refcount;
} Ejemplo;

//Constructor
void crear_Ejemplo(void *ref, size_t tamano);

//Destructor
void destruir_Ejemplo(void *ref, size_t tamano);

//TODO: Crear 3 objetos mas

typedef struct jugador{
     int anios;
     float arr[100];
     char *nombre;
}Jugador;

//Constructor
void crear_Jugador(void *ref, size_t tamano);

//Destructor
void destruir_Jugador(void *ref, size_t tamano);


typedef struct heroe{
     int salud;
     int fuerza;
     char *magia;
}Heroe;

//Constructor
void crear_Heroe(void *ref, size_t tamano);

//Destructor
void destruir_Heroe(void *ref, size_t tamano);


typedef struct malo{
     int salud;
     int fuerza;
}Malo;

//Constructor
void crear_Malo(void *ref, size_t tamano);

//Destructor
void destruir_Malo(void *ref, size_t tamano);


