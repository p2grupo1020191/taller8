flags= -I include/
obj= obj/prueba.o obj/crear_cache.o obj/destruir_cache.o obj/devolver_cache.o obj/obtener_cache.o obj/stats_cache.o obj/Ejemplo_cache.o 
staticlib= ./lib/libslaballoc.a
executableStatic= ./bin/estatico
dinamiclib= ./lib/libslaballoc.so
executableDinamic= ./bin/dinamico
#Compila el programa principal
all:   $(executableStatic) $(executableDinamic)

#Creación del ejecutable estatico (libreria estatica)
$(executableStatic): $(obj) $(staticlib)
	gcc -static $(obj) $(staticlib) -o $(executableStatic)

$(staticlib): $(obj)	
	ar -r $(staticlib) $(obj)	

./obj/%.o: ./src/%.c
	gcc -g -Wall -static -c $^ -o $@ $(flags)

#Creación del ejecutable dinamico (libreria dinamica)
$(executableDinamic): $(obj) $(dinamiclib)
	gcc -o $(executableDinamic) $(flags) $(obj) -L./lib/ -Bdynamic -lslaballoc

$(dinamiclib): $(obj)	
	ld -o $(dinamiclib) $(obj) -shared

#Creación de .o para la lib dinamica
./obj/%.o: ./src/%.c
	gcc -g -Wall  -c -fPIC $^ -o $@ $(flags)


.PHONY: clean
clean:
	rm bin/* obj/*.o
