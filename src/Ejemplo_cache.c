#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"


void crear(void *ref, size_t tamano){
    Ejemplo *buf = (Ejemplo *)ref;
    buf->a = 0;
    int i = 0;
    for(i = 0; i< 100; i++){
    	buf->b[i] = i;
    }
    buf -> msg = "Hola mundo";
}

void destruir(void *ref, size_t tamano){
    Ejemplo *buf = (Ejemplo *)ref;
    buf->a = -1;
    //buf->b = "";
    free(buf->b);
    free(buf->msg);
}

//funciones datos
void crear_Jugador(void *ref, size_t tamano){
     Jugador *buf= (Jugador *)ref;
     buf->anios = 5;
     int i=0;
     for(i=0; i<100; i++){
	buf->arr[i]=i; 
      }
     buf ->nombre = (char*) malloc(sizeof(char)*1000);
}
void destruir_Jugador(void *ref, size_t tamano){
     Jugador *buf= (Jugador *)ref;
     buf->anios=-1;
     free(buf->arr);
     free(buf->nombre);
}

//funciones heroe
void crear_Heroe(void *ref, size_t tamano){
     Heroe *buf= (Heroe *)ref;
     buf ->salud =10;
     buf ->fuerza =8;
     buf ->magia = (char*) malloc(sizeof(char)*1000);
}

void destruir_Heroe(void *ref, size_t tamano){
     Heroe *buf= (Heroe *)ref;
     buf ->salud=0;
     buf ->fuerza=0;
     free(buf-> magia);
}

//funciones malo
void crear_Malo(void *ref, size_t tamano){
     Malo *buf= (Malo *)ref;
     buf ->salud =7;
     buf ->fuerza =5;
}

void destruir_Malo(void *ref, size_t tamano){
     Malo *buf= (Malo *)ref;
     buf ->salud=0;
     buf ->fuerza=0;
}


