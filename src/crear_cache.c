#include <stdlib.h>
#include<stdio.h>
#include "slaballoc.h"
#include "objetos.h"

//Crea el cache. Esta funcion debe crear 
//1. El objeto SlabAlloc
//2. El arreglo de tamano TAMANO_CACHE de estructuras SlabAlloc
//   (este espacio debe ser creado dinamicamente, ya que podrá crecer).
//3. Reservar el espacio de memoria para cachear los objetos dado por TAMANO_CACHE * tamano_objeto
//   (este espacio debe ser creado dinamicamente, ya que podrá crecer).
//4. La funcion recibe dos punteros a funciones, que se guardan en la struct SlabAlloc
//Cuando cree el cache, ud debe llamar a la funcion constructor por cada objeto

SlabAlloc *crear_cache(char *nombre, size_t tamano_objeto, constructor_fn constructor, destructor_fn destructor){
	SlabAlloc *slbaloc= (SlabAlloc *)malloc(sizeof(SlabAlloc));
	if(slbaloc!=NULL){	
		//Definiendo los elementos de la estructura slaballoc para el ejemplo	
		slbaloc->nombre=nombre;
		slbaloc->tamano_objeto=tamano_objeto;		
		slbaloc->tamano_cache=TAMANO_CACHE;
		slbaloc->cantidad_en_uso=0;				
		slbaloc->constructor=constructor;
		slbaloc->destructor=destructor;		
		slbaloc->slab_ptr=NULL;
		slbaloc->mem_ptr=NULL; //ESte es el ptr donde se cachearan los objetos
		Slab *buf;
		buf=(Slab *)malloc(slbaloc->tamano_cache*sizeof(Slab)); //Aqui estableces el tamaño del cache como un tipo slab		
		slbaloc->slab_ptr=buf;
		void *buf1; //Tipo void
		buf1=(void *)malloc(slbaloc->tamano_cache*sizeof(tamano_objeto));
		//Se establece el tamaño del de los slabs para cada objeto, el mem ptr sirve para cachear
		slbaloc->mem_ptr=buf1;
		//Se reserva la memoria para el ptr
		slbaloc->slab_ptr->ptr_data=buf1;
		//También para el old, aunque dudo en esto ya que esto se irá estableciendo cuando se haga un realloc
		slbaloc->slab_ptr->ptr_data_old=buf1;
		for(int i=0;i<slbaloc->tamano_cache;i++){ //i tiene que ser menor al tamaño cache para ir llenando los  atributos del slab	
			(buf+i)->ptr_data=(buf1+i);
			(buf+i)->ptr_data_old=(buf1+i);
			(buf+i)->status=1; //Esta disponible cuando status=1
			slbaloc->constructor((slbaloc->slab_ptr->ptr_data),slbaloc->tamano_objeto);			
		}
	}
	else{
		printf("El SlabAlloc no está null, por lo tanto no se puede crear cache\n");
	}
	return slbaloc;
}
