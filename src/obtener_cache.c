#include <stdlib.h>
#include<stdio.h>
#include "slaballoc.h"
#include "objetos.h"

//Retorna un objeto con bandera en DISPONIBLE
//Tiene que haber slabs disponibles
//Se marca el slab como EN_USO. 
//Si la bandera cache->crecer == 1 y si ya no hay
//objetos disponibles, debe hacer crecer el cache
//al doble del tamaño anterior. Caso contrario, solo
//retorne null. No olvide crear las nuevas structs
//SlabAlloc que tendran la información de los nuevos
//objetos.
//Si ya todos los objetos estan en uso, y la bandera
// crecer == 1, duplique el tamano del cache actual.
void *obtener_cache(SlabAlloc *alloc, int crecer){
	unsigned int tamano=alloc->tamano_cache;
	for(int i=0;i<alloc->tamano_cache;i++){
		if((alloc->slab_ptr+i)->status==1){
			(alloc->slab_ptr+i)->status=0; //Ya no está disponible
			alloc->cantidad_en_uso= alloc->cantidad_en_uso+1;
			return ((alloc->slab_ptr+i)->ptr_data);
		}
	}	
	if(crecer==0){
		return NULL;
	}	
	if(crecer==1){
		alloc->tamano_cache=(alloc->tamano_cache)*2;		
		alloc->slab_ptr=(Slab *)realloc(alloc->slab_ptr,((alloc->tamano_cache)+2)*sizeof(Slab));//se hace el realloc para poder agrandar la memoria
		alloc->mem_ptr=(void*)realloc(alloc->mem_ptr,(alloc->tamano_cache+2)*sizeof(alloc->tamano_objeto));//se hace el realloc para poder agrandar la memoria
		/* En este momento se actualizará cada puntero ya que se hizo un realloc y puede ocasionar conflictos*/
		alloc->slab_ptr->ptr_data=alloc->mem_ptr;		
		for(int j=tamano;j<alloc->tamano_cache;j++){
			(alloc->slab_ptr+j)->status=1; //EL slab está disponible
			alloc->constructor(alloc->slab_ptr->ptr_data+j,alloc->tamano_objeto);	//COnstruyendo el objeto		
		}					
		for(int h=0;h<alloc->tamano_cache;h++){
			(alloc->slab_ptr+h)->ptr_data=(alloc->slab_ptr->ptr_data+h); //EStableciendo la nueva dirección
			//(alloc->slab_ptr+h)->ptr_data_old=(alloc->slab_ptr->ptr_data+h); //Seteando también el ptr_data_old
		}
		alloc->cantidad_en_uso= alloc->cantidad_en_uso+1; //MEncionando que hay un nuevo objeto en uso
		(alloc->slab_ptr+tamano)->status=0; // Ya no está disponible el slab ya que está en uso
		return (alloc->slab_ptr+tamano)->ptr_data;	
	}
	alloc->cantidad_en_uso= alloc->cantidad_en_uso+1;  //MEncionando que hay un nuevo objeto en uso
	(alloc->slab_ptr+tamano)->status=0; // Ya no está disponible el slab ya que está en uso
	return (alloc->slab_ptr+tamano)->ptr_data; // Retornando el ptr_data
}
